resource "aws_route53_zone" "zone" {
  name = "${var.domain}"
  comment = "Zone for ${var.domain}. Managed by Terraform."

  tags = merge (
    {"Name" = "${var.domain}"},
    var.tags
  )
}
