variable "domain" {
  type        = string
  description = "The domain to create the zone for"
}

variable "a_records" {
  type        = list(string)
  description = "Array of A record(s)"
}

variable "mx_records" {
  type        = list(string)
  description = "Array of MX record(s)"
}

variable "tags" {
  type        = map(string)
  description = "A map of tags to apply to all resources"
}
