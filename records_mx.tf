resource "aws_route53_record" "mx" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = var.domain
  type    = "MX"
  ttl     = "3600"
  records = var.mx_records

  allow_overwrite = true
}
