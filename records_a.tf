resource "aws_route53_record" "a_record" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = var.domain
  type    = "A"
  ttl     = "3600"
  records = var.a_records

  allow_overwrite = true
}

